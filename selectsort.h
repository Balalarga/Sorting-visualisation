#ifndef SELECTSORT_H
#define SELECTSORT_H

#include "mainsort.h"

class SelectSort : public MainSort
{
    void selectionSort();
public:
    void run();
};

#endif // SELECTSORT_H
