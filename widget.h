#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QTime>

#include "bubsort.h"
#include "insertsort.h"
#include "selectsort.h"
#include "pyramidsort.h"

class Widget : public QWidget
{
    Q_OBJECT

private:
    QPushButton* resetBtn;
    QPushButton* stopBtn;
    QVector<int> arr;
    QVector<QGraphicsScene*> scene;
    QVector<QGraphicsView*> view;
    QVector<MainSort*> sorts;

    void genArray();

public:
    Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void draw(QVector<int> arr, int num);
    void reset();
};

#endif // WIDGET_H
