#ifndef PYRAMIDSORT_H
#define PYRAMIDSORT_H

#include "mainsort.h"

class PyramidSort : public MainSort
{
    void building_pyramid(int i, int count);
public:
    void run();
};

#endif // MERGESORT_H
