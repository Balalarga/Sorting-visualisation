#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    this->resize(1200,400);
    resetBtn = new QPushButton("Refresh");
    stopBtn = new QPushButton("Stop");
    connect(resetBtn, SIGNAL(clicked(bool)), this, SLOT(reset()));
    QHBoxLayout* hLay = new QHBoxLayout();
    QVBoxLayout* vLay = new QVBoxLayout(this);
    vLay->addWidget(resetBtn);
    vLay->addWidget(stopBtn);
    sorts = {new BubSort(),
             new InsertSort(),
             new SelectSort(),
             new PyramidSort()
            };

    for(int i(0); i < sorts.count(); i++){
        auto s = new QGraphicsScene(this);
        auto v = new QGraphicsView(s);
        QLabel* name = new QLabel(sorts[i]->getName());
        auto tHLay = new QHBoxLayout();
        s->setSceneRect(0, 0, 255, width()/sorts.count());
        scene.append(s);
        view.append(v);
        tHLay->addWidget(name);
        tHLay->addWidget(v);
        hLay->addLayout(tHLay);
        connect(sorts[i], SIGNAL(draw(QVector<int>,int)), this, SLOT(draw(QVector<int>,int)));
        connect(stopBtn, SIGNAL(clicked(bool)), sorts[i], SLOT(terminate()));
    }
    vLay->addLayout(hLay);

    genArray();
}

void Widget::genArray(){
    qsrand(QDateTime::currentMSecsSinceEpoch());
    arr.clear();
    for(int i=0;i<100;i++){
        arr<<i;
    }
    std::random_shuffle(arr.begin(), arr.end());
    for(int i(0); i < sorts.count(); i++){
        draw(arr, i);
        sorts[i]->init(arr);
    }
}

void Widget::reset(){
    for(int i(0); i < sorts.count(); i++){
        if(sorts[i]->isRunning())
            sorts[i]->terminate();
    }
    genArray();
}

Widget::~Widget()
{
    for(int i(0); i < sorts.count(); i++){
        delete sorts[i];
    }
}

void Widget::draw(QVector<int> arr, int num){
    QGraphicsScene* tScene = scene[num];
    tScene->clear();
    for(int i(0); i<arr.count(); i++){
        int col = arr[i]*(255/arr.count());
        QPen pen = QPen(QColor(0, col, 255));
        QBrush brush = QBrush(QColor(0, col, 255));
        tScene->addPolygon(QPolygon(QRect(i*tScene->width()/arr.count(), 0,
                                          tScene->width()/arr.count(), tScene->height())),pen,brush);
    }
}
