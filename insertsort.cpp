#include "insertsort.h"

void InsertSort::run(){
    name = "Insertion";
    insertionSort();
}

void InsertSort::insertionSort(){
    int ind = 0;
    int temp;
    for(int i = 1; i < arr.count(); i++){
        temp = arr[i];
        ind = i-1;
        for(int j = ind; j >= 0; j--){
            if(arr[j] > temp){
                arr[j+1] = arr[j];
                arr[j] = temp;
                emit draw(arr, 1);
                usleep(7000);
            }
        }
    }
}
