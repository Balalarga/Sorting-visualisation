#ifndef BUBSORT_H
#define BUBSORT_H

#include "mainsort.h"

class BubSort: public MainSort
{
    void bubbleSort();
public:
    void run();
};

#endif // SORTS_H
