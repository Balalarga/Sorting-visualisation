#ifndef INSERTSORT_H
#define INSERTSORT_H

#include "mainsort.h"

class InsertSort : public MainSort
{
    void insertionSort();
public:
    void run();
};

#endif // INSERTSORT_H
