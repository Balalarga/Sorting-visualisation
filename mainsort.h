#ifndef MAINSORT_H
#define MAINSORT_H

#include <QThread>
#include <QVector>

class MainSort : public QThread
{
    Q_OBJECT
protected:
    QVector<int> arr;
    QString name;

public:
    virtual void run() = 0;
    void init(QVector<int>& nArr);
    QString getName();

signals:
    void draw(QVector<int>, int);
};

#endif // MAINSORT_H
