#include "mainsort.h"

void MainSort::init(QVector<int>& nArr)
{
    arr = nArr;
    qRegisterMetaType<QVector<int>>("QVector<int>");
    start();
}

QString MainSort::getName(){
    return name;
}
