#include "bubsort.h"

void BubSort::run(){
    name = "Bubble";
    bubbleSort();
}

void BubSort::bubbleSort(){
    int temp;
    for(int i(0); i < arr.count(); i++){
        for(int j = arr.count()-1 ; j >= i+1 ; j--){
            if(arr[j] < arr[j-1]){
                temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
                emit draw(arr, 0);
                usleep(7000);
            }
        }
    }
}
