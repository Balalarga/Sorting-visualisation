#include "selectsort.h"

void SelectSort::run(){
    name = "Selection";
    selectionSort();
}

void SelectSort::selectionSort(){
    int j = 0;
    int tmp = 0;
    for(int i=0; i<arr.count(); i++){
      j = i;
      for(int k = i; k<arr.count(); k++){
        if(arr[j]>arr[k]){
          j = k;
        }
      }
      tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
      emit draw(arr, 2);
      usleep(50000);
    }
}
