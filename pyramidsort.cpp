#include "pyramidsort.h"

void PyramidSort::run(){
    name = "Pyramid";
    for(int i=arr.count()/2;i>=0;i--){
        building_pyramid(i,arr.count()-1);
    }
    for(int i=arr.count()-1;i>0;i--){
        int temp=arr[i];
        arr[i]=arr[0];
        arr[0]=temp;
        building_pyramid(0,i-1);
    }
}

void PyramidSort::building_pyramid(int i, int count){
    int child;
    int new_elem=arr[i];
    while(i<=count/2){
        child=2*i;
        if(child<count && arr[child]<arr[child+1])child++;
        if(new_elem>=arr[child]) break;
        arr[i]=arr[child];
        i=child;
    }
    arr[i]=new_elem;
    emit draw(arr, 3);
    usleep(50000);
}
